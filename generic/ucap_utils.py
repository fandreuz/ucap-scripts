import argparse

from common import get_node_configuration

parser = argparse.ArgumentParser(description="UCAP utility CLI")
parser.add_argument("--port", action="store_true")
parser.add_argument("--host", action="store_true")
parser.add_argument("--metrics", action="store_true")
parser.add_argument("-n", "--node", type=str, required=True)
args = parser.parse_args()

node_configuration = get_node_configuration(args.node)

if args.port:
    print(node_configuration["port"])
if args.host:
    print(node_configuration["host"])
