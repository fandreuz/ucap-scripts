import argparse
import re

import requests

from common import get_node_configuration

parser = argparse.ArgumentParser(description="UCAP metrics CLI")
parser.add_argument("--keys", action="store_true")
parser.add_argument("-n", "--node", type=str, required=True)
args = parser.parse_args()

node_configuration = get_node_configuration(args.node)

r = requests.get(
    f'https://{node_configuration["host"]}:{node_configuration["port"]}/actuator/prometheus'
)

if args.keys:
    lines = filter(lambda s: not s.startswith("#"), r.text.splitlines())
    pattern = re.compile("(.*) *")
    matches = filter(lambda o: o is not None, map(pattern.match, lines))
    keys = map(lambda m: m.group(1), matches)
    print("\n".join(keys))
else:
    print(r.text)