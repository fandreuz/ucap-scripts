import pyrda3


def get_node_configuration(node_id: str):
    builder = pyrda3.ClientServiceBuilder.new_instance()
    client = builder.build()

    device = node_id.upper().replace("-", ".")
    ap = client.get_access_point(device, "Configuration")
    return ap.get().data
