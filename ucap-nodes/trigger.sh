#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "$(readlink -f "${BASH_SOURCE[0]}")" )" && pwd)"

source $SCRIPT_DIR/vars.sh
source $SCRIPT_DIR/secrets.sh

PIPELINE_ID=$(curl --silent \
     --request POST \
     --form token=$GITLAB_TOKEN \
     --form ref=$BRANCH_NAME \
     --form variables[NODE_PATTERN]="$1" \
     --form variables[NODE_ENVIRONMENT]="$2" \
     "https://gitlab.cern.ch/api/v4/projects/${UCAP_NODES_PROJECT_ID}/trigger/pipeline" | jq '.id')
$BROWSER https://gitlab.cern.ch/acc-co/ucap/ucap-nodes/-/pipelines/$PIPELINE_ID
